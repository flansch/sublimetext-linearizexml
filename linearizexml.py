import sublime, sublime_plugin, re

class LinearizeXmlCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		# handle all selected regions
		has_region = False
		for region in self.view.sel():
			if not region.empty():
				has_region = True
				# Get the selected text
				s = self.view.substr(region)
				# Remove whitespace between tags
				s = re.sub (r">\s+<", "><", s)
				# Replace the selection with transformed text
				self.view.replace(edit, region, s)

		# if no region selected than handle complete file content
		if not has_region:
			region = sublime.Region(0, self.view.size())
			# Get the selected text
			s = self.view.substr(region)
			# Remove whitespace between tags
			s = re.sub (r">\s+<", "><", s)
			# Replace the selection with transformed text
			self.view.replace(edit, region, s)
